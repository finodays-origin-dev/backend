package db

import (
	"fmt"
	"gorm.io/gorm"
)

type FaceRecognition struct {
	gorm.Model
	Image       string  `json:"image" db:"image"`
	Emotion     string  `json:"emotion"`
	EmotionRate float64 `json:"emotion_rate"`
	MaskStatus  int     `json:"mask_status"`
}

func (FaceRecognition) TableName() string {
	return "face_recognitions"
}

func AddRecognition(d *FaceRecognition) error {
	return db.Model(FaceRecognition{}).Create(&d).Error
}

func UpdateRecognition(d *FaceRecognition) error {
	q := fmt.Sprintf("update face_recognitions set image='%s' where id='%d'", d.Image, d.ID)
	return db.Exec(q).Error
}

func GetAll(start, end string, offset, limit int) (result []FaceRecognition, err error) {
	err = db.Raw(`select * from face_recognitions where created_at >= ? and created_at <= ? offset ? limit ?`, start, end, offset, limit).Find(&result).Error
	return
}
