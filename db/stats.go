package db

type EmotionChartStat struct {
	Name  string `json:"name"`
	Value int    `json:"value"`
}

func GetEmotionChartStats(start, end string) (result []EmotionChartStat, err error) {
	err = db.Raw(`select emotion as name, count(*) as value
from face_recognitions
where created_at >= ? and created_at <= ?
group by emotion`, start, end).Find(&result).Error
	return
}

type MaskChartStat struct {
	Name string `json:"name"`
	None int    `json:"none"`
	Full int    `json:"full"`
	All  int    `json:"all"`
}

func GetMaskChartStats(start, end string) (result []MaskChartStat, err error) {
	err = db.Raw(`select to_char(date_trunc('hour', created_at) +
               make_interval(0, 0, 0, 0, 0, (extract(minute from created_at)::int / 30) * 30),
               'YYYY-MM-DD HH24:MI')               as name,
       count(case when mask_status = 0 then 1 end) as none,
       count(case when mask_status = 1 then 1 end) as full,
       count(*)                                    as all
from face_recognitions
where created_at >= ?
  and created_at <= ?
group by 1
order by 1;`, start, end).Find(&result).Error
	return
}
