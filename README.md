# Backend

## Как запускать

`go run main.go`

## Example envs

```
DB_HOST=;
DB_NAME=finodays;
DB_PASSWORD=;
DB_PORT=5432;
DB_USER=finodays;
PYTHON_URL=http://localhost:5000/faces_api
```

Где PYTHON_URL это ссылка на сервис faces_api (другой репозиторий)