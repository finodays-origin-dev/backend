package main

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/semyon-dev/finodays/controllers"
	"github.com/semyon-dev/finodays/db"
	"log"
	"net/http"
	"time"
)

func main() {
	db.Connect()
	router := gin.Default()
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"PUT", "PATCH", "POST", "FETCH", "GET", "DELETE"},
		AllowHeaders:     []string{"Origin", "Content-Type", "Content-Length"},
		ExposeHeaders:    []string{"Content-Length", "Content-Type"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	router.POST("/api/upload", controllers.UploadRecognition)
	router.GET("/api/stats/moods", controllers.EmotionChart)
	router.GET("/api/stats/masks", controllers.MaskChart)
	router.GET("/api/visits", controllers.Visits)

	err := http.ListenAndServe(":8080", router)
	if err != nil {
		log.Println(err)
		return
	}
}
