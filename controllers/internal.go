package controllers

import (
	"bytes"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/semyon-dev/finodays/db"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

func UploadRecognition(c *gin.Context) {
	//file, header, err := c.Request.FormFile("file")
	// Single file
	file, err := c.FormFile("upload.png")
	if err != nil {
		log.Println("c.FormFile: ", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "smth wrong in get file on server: " + err.Error(),
		})
		return
	}

	open, err := file.Open()
	if err != nil {
		c.JSON(500, gin.H{
			"result": err.Error(),
		})
		return
	}

	photoBody, err := io.ReadAll(open)
	if err != nil {
		c.JSON(500, gin.H{
			"result": err.Error(),
		})
		return
	}

	resp, err := http.Post(os.Getenv("PYTHON_URL"), "image/jpeg", bytes.NewBuffer(photoBody))
	if err != nil {
		c.JSON(500, gin.H{
			"result": err.Error(),
		})
		return
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		c.JSON(500, gin.H{
			"result": err.Error(),
		})
		return
	}

	type Emotions struct {
		Emotion string    `json:"emotion"`
		Scores  []float64 `json:"scores"`
		Mask    []float64 `json:"mask"`
	}

	var e Emotions
	err = json.Unmarshal(body, &e)
	if err != nil {
		log.Println(err)
		c.JSON(500, gin.H{
			"result": e,
		})
		return
	}

	e.Emotion = strings.ToLower(e.Emotion)

	var max float64 = 0
	for _, v := range e.Scores {
		if v > max {
			max = v
		}
	}

	recog := db.FaceRecognition{
		Emotion:     e.Emotion,
		EmotionRate: max,
		MaskStatus:  0,
	}

	// none, wrong, full
	var mask = "none"
	if e.Mask[0] < e.Mask[1] {
		mask = "full"
		recog.MaskStatus = 1
	}

	if e.Mask[0] < 0 && e.Mask[1] < 0 {
		mask = "none"
		recog.MaskStatus = 0
	}

	err = db.AddRecognition(&recog)
	if err != nil {
		c.JSON(500, gin.H{
			"result": err.Error(),
		})
		return
	}

	go func(recog2 *db.FaceRecognition) {
		// Upload the file to specific dst.
		err = c.SaveUploadedFile(file, "./photos/"+strconv.Itoa(int(recog2.ID))+file.Filename)
		if err != nil {
			log.Println("SaveUploadedFile " + err.Error())
		}
		recog2.Image = "/photos/" + strconv.Itoa(int(recog2.ID)) + file.Filename
		err = db.UpdateRecognition(recog2)
		if err != nil {
			log.Println(err)
		}
	}(&recog)

	c.JSON(http.StatusOK, gin.H{
		"emotion":      e.Emotion,
		"emotion_rate": max,
		"mask":         mask,
		"mask_rate":    e.Mask,
	})
}
