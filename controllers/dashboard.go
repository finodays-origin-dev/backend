package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/semyon-dev/finodays/db"
	"net/http"
	"strconv"
	"time"
)

func EmotionChart(c *gin.Context) {

	startRaw := c.Query("start") // 2022-01-01
	endRaw := c.Query("end")

	_, err := time.Parse("2006-01-02", startRaw)
	if err != nil {
		c.JSON(400, gin.H{
			"result": err.Error(),
		})
		return
	}

	_, err = time.Parse("2006-01-02", endRaw)
	if err != nil {
		c.JSON(400, gin.H{
			"result": err.Error(),
		})
		return
	}

	values, err := db.GetEmotionChartStats(startRaw, endRaw)
	if err != nil {
		c.JSON(500, gin.H{
			"result": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"result": values,
	})
}

func MaskChart(c *gin.Context) {

	startRaw := c.Query("start") // 2022-01-01
	endRaw := c.Query("end")

	_, err := time.Parse("2006-01-02", startRaw)
	if err != nil {
		c.JSON(400, gin.H{
			"result": err.Error(),
		})
		return
	}

	_, err = time.Parse("2006-01-02", endRaw)
	if err != nil {
		c.JSON(400, gin.H{
			"result": err.Error(),
		})
		return
	}

	values, err := db.GetMaskChartStats(startRaw, endRaw)
	if err != nil {
		c.JSON(500, gin.H{
			"result": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"result": values,
	})
}

func Visits(c *gin.Context) {

	startRaw := c.Query("start") // 2022-01-01
	endRaw := c.Query("end")
	limit, _ := strconv.Atoi(c.Query("limit"))
	offset, _ := strconv.Atoi(c.Query("offset"))
	if limit == 0 {
		limit = 100
	}

	_, err := time.Parse("2006-01-02", startRaw)
	if err != nil {
		c.JSON(400, gin.H{
			"result": err.Error(),
		})
		return
	}

	_, err = time.Parse("2006-01-02", endRaw)
	if err != nil {
		c.JSON(400, gin.H{
			"result": err.Error(),
		})
		return
	}

	values, err := db.GetAll(startRaw, endRaw, offset, limit)
	if err != nil {
		c.JSON(500, gin.H{
			"result": err.Error(),
		})
		return
	}

	//for i, _ := range values {
	//	values[i].ImageBase64 = base64.StdEncoding.EncodeToString(values[i].Image)
	//}

	c.JSON(http.StatusOK, gin.H{
		"result": values,
	})
}
